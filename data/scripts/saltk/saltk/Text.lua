local Widget = require("scripts/saltk/saltk/Widget")

local default_text = {
    text = "Text",
    font = "8_bit",
    font_size = 12,
    color = {255, 255, 255},
    halign = "left",
    valign = "top",
    should_fit = false,
    overflow = true,
    text_overflow = "...",
    render = function (text)
        -- Delete font_s if already exists
        if text.font_s then
            text.surface:clear()
        end
        if text.scale then
            text.font_s = sol.text_surface.create({
                font = text.font,
                font_size = text.font_size * text.width_scale,
                color = text.color,
                text = text.text,
            })
        end
        local x = 0
        local y = 4
        local t_x, t_y = text.surface:get_size()
        local tt_x, tt_y = text.font_s:get_size()

        if text.should_fit then
            while t_x < tt_x do
                text.font_s = sol.text_surface.create({
                    font = text.font,
                    font_size = text.font_size * text.width_scale,
                    color = text.color,
                    text = text.text,
                })
                tt_x, tt_y = text.font_s:get_size()
                text.font_size = text.font_size - 1
            end
        end
        if not text.overflow then
            while t_x < tt_x do
                text.text = text.text:sub(1, -5)
                text.text = text.text .. text.text_overflow
                text.font_s = sol.text_surface.create({
                    font = text.font,
                    font_size = text.font_size * text.width_scale,
                    color = text.color,
                    text = text.text,
                })
                tt_x, tt_y = text.font_s:get_size()
            end
        end


        if text.halign == "center" then
            x = (t_x - tt_x) / 2
        elseif text.halign == "right" then
            x = tt_y - t_x 
        end
        if text.valign == "middle" then
            y = (t_y - tt_y)
        elseif text.valign == "bottom" then
            y = tt_x - t_y
        end
        text.font_s:draw(text.surface, x, y)
    end
}
    
local function new(o)
    local o = o or {}
    o.text = o.text or default_text.text
    o.font = o.font or default_text.font
    o.font_size = o.font_size or default_text.font_size
    o.color = o.color or default_text.color
    o.render = o.render or default_text.render
    o.should_fit = o.should_fit
    if o.should_fit == nil then
        o.should_fit = default_text.should_fit
    end

    o.overflow = o.overflow
    if o.overflow == nil then
        o.overflow = default_text.overflow
    end
    o.text_overflow = o.text_overflow or default_text.text_overflow
    o.font_s = sol.text_surface.create({
        font = o.font,
        font_size = o.font_size,
        color = o.color,
        text = o.text
    })
    
    o = Widget.new(o)

    return o
end

local Text = {
    new = new
}

return Text