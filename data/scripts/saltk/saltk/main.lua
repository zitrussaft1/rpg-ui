
-- Import Widget from current directory
DEVEL_DIR="scripts/saltk/"

--[[function sol.video:get_scale()
    local screen_width, screen_height = self:get_window_size()
    local quest_width, quest_height = self:get_quest_size()
    local width_scale = screen_width/quest_width
    local height_scale = screen_height/quest_height
    print("scale", width_scale, height_scale)
    if width_scale > height_scale then
        return height_scale
    else
        return width_scale
    end
end]]

local Widget = require("scripts/saltk/saltk/Widget")
local Grid = require("scripts/saltk/saltk/Grid")
local Text = require("scripts/saltk/saltk/Text")
local Image = require("scripts/saltk/saltk/Image")

require("scripts/saltk/utils")
local game = sol.main.get_metatable("game")

local widgets = {}

local function init(context, id, surface, all_pixels) -- See https://doxygen.solarus-games.org/latest/lua_api_menu.html
    local menus = {}
    local menu = {}

    -- Tree to inverse ordered list
    function tree_to_inverse(tree, uid)
        local list = {}
        local uid = uid or ""
        for _, child in pairs(tree) do
            if child.can_be_selected then
                table.insert(list, 1, {
                    child = child,
                    pos_x = child.position[1],
                    pos_y = child.position[2],
                    size_x = child.size[1],
                    size_y = child.size[2],
                })
            end
            if child.children then
                local sublist = tree_to_inverse(child.children, uid)
                for _, subchild in pairs(sublist) do
                    if subchild.child.can_be_selected then
                        table.insert(list, 1, {
                            child = subchild.child,
                            pos_x = subchild.pos_x, --+ child.position[1],
                            pos_y = subchild.pos_y, -- + child.position[2],
                            size_x = subchild.size_x,
                            size_y = subchild.size_y,
                        })
                    end
                end
            end
        end
        return list
    end

    local mouse_pos_list = tree_to_inverse({surface})


    -- Sort by position
    local keyboard_pos_list = tree_to_inverse({surface})
    
    table.sort(keyboard_pos_list, function(a, b)
        return a.pos_y < b.pos_y or (a.pos_y == b.pos_y and a.pos_x < b.pos_x)
    end)

    -- Startup stuff
    if game.get_saltk_menus then
        menus = game:get_saltk_menus()
    end

    -- Save last selected widget
    local last_selected_widget = nil
    
    -- Save last mouse position
    local last_mouse_x, last_mouse_y = sol.input.get_mouse_position()
    
    local function draw(asd, dst_surface)
        if not sol.menu.is_started(menu) then
            return
        end
        Widget.draw(surface, dst_surface)
        -- Get mouse position and select widget
        local mouse_x, mouse_y = sol.input.get_mouse_position()
        if mouse_x ~= last_mouse_x or mouse_y ~= last_mouse_y then
            last_mouse_x, last_mouse_y = mouse_x, mouse_y
            for _, child in pairs(mouse_pos_list) do
                if mouse_x >= child.pos_x and mouse_x <= child.pos_x + child.size_x and
                    mouse_y >= child.pos_y and mouse_y <= child.pos_y + child.size_y then
                    if last_selected_widget ~= nil then
                        Widget.unselect(last_selected_widget)
                    end
                    last_selected_widget = Widget.get_child_by_map(surface, child.child.map)
                    Widget.select(last_selected_widget)
                    break
                end
            end
        end
    end

    function menu:on_draw(dst_surface)
        if not all_pixels then
            draw("", dst_surface)
        end
    end

    function menu:on_key_pressed(key)
        if last_selected_widget ~= nil then
            local tmp = last_selected_widget:on_key_press(key)
            if tmp ~= nil then
                if last_selected_widget ~= nil then
                    Widget.unselect(last_selected_widget)
                end
                last_selected_widget = Widget.get_child_by_map(surface, tmp.map)
                Widget.select(last_selected_widget)
                return
            end
        end
        if last_selected_widget ~= nil and last_selected_widget.parent ~= nil and
            Table:contains({"up", "down", "left", "right"}, key) then
            last_selected_widget = last_selected_widget.parent:next_on_key_press(key)
        elseif (last_selected_widget == nil or last_selected_widget.parent == nil) and
            Table:contains({"up", "down", "left", "right"}, key) then
            for _, child in pairs(keyboard_pos_list) do
                if child.child.can_be_selected then
                    if last_selected_widget ~= nil then
                        Widget.unselect(last_selected_widget)
                    end
                    last_selected_widget = Widget.get_child_by_map(surface, child.child.map)
                    Widget.select(last_selected_widget)
                elseif child.child.children ~= nil then
                    local function recursive_select_first(widget)
                        for _, subchild in pairs(widget.children) do
                            if subchild.can_be_selected then
                                if last_selected_widget ~= nil then
                                    Widget.unselect(last_selected_widget)
                                end
                                last_selected_widget = Widget.get_child_by_map(surface, subchild.map)
                                Widget.select(last_selected_widget)
                                return
                            elseif subchild.children ~= nil then
                                recursive_select_first(subchild)
                            end
                        end
                    end
                    recursive_select_first(child.child)
                end
            end
        end
    end

    menus[id] = menu 
    widgets[id] = surface

    if all_pixels then
        if sol.video.on_draw then
            local new_draw = function(self, ...)
                self:on_draw(...)
                self:draw(...)
            end
            sol.video.on_draw = new_draw
        else
            sol.video.on_draw = draw
        end
    end

    function game:set_saltk_enabled(id, enabled)
        if enabled then
            self:set_paused(true)
            sol.menu.start(context, menus[id])
            sol.menu.bring_to_front(menus[id])
        else
            self:set_paused(false)
            sol.menu.stop(menus[id])
        end
    end

    function game:is_saltk_enabled(id)
        return sol.menu.is_started(menus[id])
    end

    function game:get_saltk_menus()
        return menus
    end

    function game:get_saltk_widget(menu_id)
        return widgets[menu_id]
    end
end

return {
    Widget = Widget,
    Grid = Grid,
    Text = Text,
    Image = Image,
    init = init,
}