local Widget = require("scripts/saltk/saltk/Widget")

local default_grid = {
    grid = {4, 4},
    grid_cell_sizes = {},
    padding = {16, 16},
    render = function (grid)
        if grid.background_color then
            grid.surface:fill_color(grid.background_color)
        end
    end,
    next_on_key_press = function(grid, key)
        local result = nil
        if key == "up" then
            result = grid:iter_selectable(grid.grid[1] * -1)
        elseif key == "down" then
            result = grid:iter_selectable(grid.grid[1])
        elseif key == "left" then
            result = grid:iter_selectable(-1)
        elseif key == "right" then
            result = grid:iter_selectable(1)
        end
        return result
    end
}

local function new(o)
    local o = o or {}
    o.grid = o.grid or default_grid.grid
    o.padding = o.padding or default_grid.padding
    o.render = o.render or default_grid.render
    o.next_on_key_press = o.next_on_key_press or default_grid.next_on_key_press
    o = Widget.new(o)
    return o
end

local function add_child(grid, child)
    child.size = {
        grid.size[1] / grid.grid[1] - grid.padding[1],
        grid.size[2] / grid.grid[2] - grid.padding[2]
    }

    local number_of_children = 0
    if grid.children then
        number_of_children = #grid.children
    end
    local x = number_of_children % grid.grid[1]
    local y = math.floor(number_of_children / grid.grid[1])

    local padding_should = x 
    child.position = {
        grid.position[1] + (x * child.size[1]) + (grid.padding[1] * (x + 1.5) / 2),
        grid.position[2] + (y * child.size[2]) + (grid.padding[2] * (y + 1.5) / 2)
    }

    child.position_s = {
        grid.position_s[1] + (x * child.size[1]) + (grid.padding[1] * (x + 1.5) / 2),
        grid.position_s[2] + (y * child.size[2]) + (grid.padding[2] * (y + 1.5) / 2)
    }

    Widget.update_surface(child)
    -- Update children recursively
    if child.children then
        local children = child.children
        Widget.remove_all_children(child)
        for _, c in pairs(children) do
            add_child(child, c)
        end
    end

    Widget.add_child(grid, child, false)
end

local Grid = {
    new = new,
    add_child = add_child
}

return Grid