local default_widget = {
    position = {0, 0},
    padding = {0, 0},
    size = {320, 240},
    background_color = nil,
    visible = true,
    children = nil,
    selected = false,
    is_child_selected = false,
    can_be_selected = false,
    child_index = 1,
    scale = false,
    parent = nil,
    render = function (widget)
        if widget.background_color then
            widget.surface:fill_color(widget.background_color)
        end
    end,
    select_first = function(widget)
        for i, child in pairs(widget.children) do
            if child.can_be_selected then
                child.selected = true
                widget.child_index = i
                return child
            end
        end
    end,
    select_last = function(widget)
        for i = #widget.children, 1, -1 do
            if widget.children[i].can_be_selected then
                widget.children[i].selected = true
                widget.child_index = i
                return widget.children[i]
            end
        end
    end,
    iter_selectable = function (widget, skip)
        local n = #widget.children
        local skip = skip or 1
        local i = widget.child_index
        -- If none selected, select first
        if widget.children[i] then
            if not widget.children[i].selected then
                widget.children[i].selected = true
                return widget.children[i]
            end
        end
        widget.children[widget.child_index].selected = false
        i = i + skip
        -- Run through all children
        if widget.children[i] then
            while (i < n and i > 0 and not widget.children[i].can_be_selected) do
                -- If on a grid, the skip variable can be used to skip to the next
                i = i + skip
            end
            -- Handle overflow
            if i > n then
                return widget:select_first()
            elseif i < 1 then
                return widget:select_last()
            end
            -- If the next child is selectable, select it
            widget.children[i].selected = true
            widget.child_index = i
            return widget.children[i]
        elseif i > n then
            return widget:select_first()
        elseif i < 1 then
            return widget:select_last()
        end
    end,
    next_on_key_press = function(widget, key)
        local result = widget.iter_selectable(widget, 0)
        if key == "up" then
            result = widget.iter_selectable(widget, -1)
        elseif key == "down" then
            result = widget.iter_selectable(widget, 1)
        end
        return result
    end,
    on_key_press = function(widget, key)
        if key == "return" then
            print("Enter pressed on "..widget.id)
        end
    end,
}

local function new(o)
    local o = o or {}
    o.id = o.id
    o.position = o.position or default_widget.position
    o.size = o.size or default_widget.size
    if o.visible == nil then
        o.visible = default_widget.visible
    else
        o.visible = o.visible
    end
    o.children = o.children or default_widget.get_children
    if o.selected == nil then
        o.selected = default_widget.selected
    else
        o.selected = o.selected
    end
    if o.is_child_selected == nil then
        o.is_child_selected = default_widget.is_child_selected
    else
        o.is_child_selected = o.is_child_selected
    end
    if o.can_be_selected == nil then
        o.can_be_selected = default_widget.can_be_selected
    else
        o.can_be_selected = o.can_be_selected
    end
    o.child_index = o.child_index or default_widget.child_index
    o.parent = o.parent or default_widget.parent
    o.padding = o.padding or default_widget.padding
    o.map = o.id

    o.surface = sol.surface.create(o.size[1], o.size[2])

    o.render = o.render or default_widget.render
    o.iter_selectable = o.iter_selectable or default_widget.iter_selectable
    o.next_on_key_press = o.next_on_key_press or default_widget.next_on_key_press
    o.select_first = o.select_first or default_widget.select_first
    o.select_last = o.select_last or default_widget.select_last

    o.scale = o.scale or default_widget.scale
    if o.scale == nil and o.parent ~= nil then
        o.scale = o.parent.scale
    end

    o.position_s = {o.position[1], o.position[2]}
    o.size_s = {o.size[1], o.size[2]}
    o.padding_s = {o.padding[1], o.padding[2]}

    o.background_color = o.background_color or default_widget.background_color

    o.on_key_press = o.on_key_press or default_widget.on_key_press

    return o
end

local function update_surface(widget)
    widget.surface = sol.surface.create(widget.size_s[1], widget.size_s[2])
end

local function scale_xy(widget, scale_x, scale_y)
    widget.position_s[1] = widget.position[1] * scale_x
    widget.position_s[2] = widget.position[2] * scale_y
    widget.size_s[1] = widget.size[1] * scale_x
    widget.size_s[2] = widget.size[2] * scale_y
    widget.padding_s[1] = widget.padding[1] * scale_x
    widget.padding_s[2] = widget.padding[2] * scale_y
    update_surface(widget)
end

local function draw(widget, surface)
    if widget.visible then
        if widget.scale then 
            local screen_width, screen_height = surface:get_size()
            local quest_width, quest_height = sol.video.get_quest_size()

            -- Create dialog scales
            widget.width_scale = screen_width/quest_width
            widget.height_scale = screen_height/quest_height

            if (widget.size_s[1] ~= widget.size[1] * widget.width_scale or
                widget.size_s[2] ~= widget.size[2] * widget.height_scale) then
                scale_xy(widget, widget.width_scale, widget.height_scale)
            end
        end
        widget:render()
        widget.surface:draw(surface, widget.position_s[1] + widget.padding_s[1], widget.position_s[2] + widget.padding_s[2])
        
        -- Draw around selected widget
        if widget.selected then
            local border_top = widget.position_s[2] - widget.padding_s[2] - 2
            local border_bottom = widget.position_s[2] + widget.size_s[2] + widget.padding_s[2] + 2
            local border_left = widget.position_s[1] - widget.padding_s[1] - 2
            local border_right = widget.position_s[1] + widget.size_s[1] + widget.padding_s[1] + 2

            local border_top_rectangle = sol.surface.create(widget.size_s[1] + widget.padding_s[1] + 4, 2)
            local border_bottom_rectangle = sol.surface.create(widget.size_s[1] + widget.padding_s[1] + 4, 2)
            local border_left_rectangle = sol.surface.create(2, widget.size_s[2] + widget.padding_s[2] + 2)
            local border_right_rectangle = sol.surface.create(2, widget.size_s[2] + widget.padding_s[2] + 2)

            local border_color = {0,0,0}

            border_top_rectangle:fill_color(border_color)
            border_bottom_rectangle:fill_color(border_color)
            border_left_rectangle:fill_color(border_color)
            border_right_rectangle:fill_color(border_color)

            border_top_rectangle:draw(surface, border_left, border_top)
            border_bottom_rectangle:draw(surface, border_left, border_bottom -2)
            border_left_rectangle:draw(surface, border_left, border_top)
            border_right_rectangle:draw(surface, border_right - 2, border_top)
        end


        if widget.children ~= nil then
            for _, child in pairs(widget.children) do
                draw(child, surface)
            end
        end
    end
end

local function set_scale(widget, scale)
    widget.scale = scale
    if widget.children ~= nil then
        for _, child in pairs(widget.children) do
            set_scale(child, scale)
        end
    end
end

local function add_child(widget, child, position)
    widget.children = widget.children or {}
    child.parent = widget
    child.map = widget.map .. "." .. child.id
    if position ~= false then
        child.position = {
            widget.position[1] + child.position[1], 
            widget.position[2] + child.position[2]
        }
        child.position_s = {
            widget.position_s[1] + child.position_s[1],
            widget.position_s[2] + child.position_s[2]
        }
    end
    if child.scale ~= widget.scale then
        set_scale(child, widget.scale)
    end
    table.insert(widget.children, child)
end

local function remove_child(widget, child)
    if widget.children then
        for i, c in pairs(widget.children) do
            if c == child then
                table.remove(widget.children, i)
                break
            end
        end
    end
end

local function remove_all_children(widget)
    widget.children = {}
end

local function set_position(widget, x, y)
    widget.position = {x, y}
end

local function set_size(widget, w, h)
    widget.size = {w, h}
    widget.surface = sol.surface.create(w, h)
end

local function set_visible(widget, visible)
    widget.visible = visible
end

local function get_position(widget)
    return widget.position
end

local function get_size(widget)
    return widget.size
end

local function get_visible(widget)
    return widget.visible
end

local function get_surface(widget)
    return widget.surface
end

local function get_children(widget)
    return widget.children
end

local function get_child_by_id(widget, id)
    if widget.children then
        for _, child in pairs(widget.children) do
            if child.id == id then
                return child
            end
        end
    end
    return nil
end

local function get_child_by_map(widget, map)
    local function gcmr(widget, map_array)
        if #map_array == 1 and map_array[1] == widget.id then
            return widget
        elseif #map_array > 1 and widget.children then
            local child = get_child_by_id(widget, map_array[2])
            table.remove(map_array, 1)
            return gcmr(child, map_array)
        else
            return nil
        end
    end
    local map_array = {}
    for s in string.gmatch(map, "[^%.]+") do
        table.insert(map_array, s)
    end
    return gcmr(widget, map_array)
end

local function get_child_by_id_recursive(widget, id)
    if widget.children then
        for _, child in pairs(widget.children) do
            if child.id == id then
                return child
            else
                local child = get_child_by_id_recursive(child, id)
                if child then
                    return child
                end
            end
        end
    end
    if widget.id == id then
        return widget
    end
    return nil
end

local function get_child_by_index(widget, index)
    if widget.children then
        return widget.children[index]
    end
    return nil
end

local function get_child_index(widget, child)
    if widget.children then
        for i, c in pairs(widget.children) do
            if c == child then
                return i
            end
        end
    end
    return nil
end

local function get_child_by_selected(widget)
    if widget.children then
        for _, child in pairs(widget.children) do
            if child.selected then
                return child
            end
        end
    end
    return nil
end

local function select(widget)
    if widget.parent then
        local parent = widget.parent
        parent.child_index = get_child_index(parent, widget)
    end
    widget.selected = true
end

local function unselect(widget)
    widget.selected = false
end
        
local function destroy(widget)
    remove_all_children(widget)
    widget = nil
end

local Widget = {
    new = new,
    draw = draw,
    render = render,
    draw_children = draw_children,
    add_child = add_child,
    remove_child = remove_child,
    remove_all_children = remove_all_children,
    set_position = set_position,
    set_size = set_size,
    set_visible = set_visible,
    get_position = get_position,
    get_size = get_size,
    get_visible = get_visible,
    get_surface = get_surface,
    get_children = get_children,
    get_child_by_id = get_child_by_id,
    get_child_by_id_recursive = get_child_by_id_recursive,
    get_child_by_map = get_child_by_map,
    get_child_by_index = get_child_by_index,
    get_children_iter = get_children_iter,
    get_child_index = get_child_index,
    iter_selectable = iter_selectable,
    next_on_key_press = next_on_key_press,
    destroy = destroy,
    select = select,
    unselect = unselect,
    update_surface = update_surface
}

return Widget
