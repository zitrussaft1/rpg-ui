
local Widget = require("scripts/saltk/saltk/Widget")

local default_image = {
    image = "animals/birds.png",
    halign = "left",
    valign = "top",
    render = function (image)
        if image.scale then
            local scale_x = image.size[1] / image.image_s_x
            local scale_y = image.size[2] / image.image_s_y
            image.image_s:set_scale(
                scale_x * image.width_scale,
                scale_y * image.height_scale
            )
        end

        image.image_s:draw(image.surface, x, y)
    end
}

local function new(o)
    local o = o or {}
    o.image = o.image or default_image.image
    o.image_s = sol.surface.create(o.image)
    o.image_s_x, o.image_s_y = o.image_s:get_size()
    o.render = o.render or default_image.render
    o.halign = o.halign or default_image.halign
    o.valign = o.valign or default_image.valign

    o = Widget.new(o)

    return o
end

local Image = {
    new = new
}

return Image