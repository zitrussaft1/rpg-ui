-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/saltk/features")

-- Features can be enabled to disabled independently by commenting
-- or uncommenting lines below.

require("scripts/saltk/hud/hud")
require("scripts/saltk/menus/dialog_box")

return true