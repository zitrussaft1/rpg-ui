-- Lua script of map first_map.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  require("scripts/test_menu/main")(game, 10, 5)
  -- You can initialize the movement and sprites of various
  -- map entities here.
end

function fakemon:on_interaction()
  local saltk = require("scripts/saltk/saltk/main")
  local widgets = game:get_saltk_widget("test")


  widgets.hp1 = 10
  widgets.hp2 = 5
  if widgets then
    saltk.Widget.get_child_by_id(widgets, "battle_log").text = "Choose a skill"
    saltk.Widget.get_child_by_id(widgets, "battle_log").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "battle_log_bg").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "options").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "bg").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "1").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "2").surface:fade_in(50, function () end)
  end

  game:set_saltk_enabled("test", true)
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end