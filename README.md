# RPG UI In Solarus

I'll be slowly updating this project as time allows, currently is not personal priority.
The grand idea is to eventually implement a full RPG System similar to RPGMaker's in separate repos 
(including potions, weapons, skills, stats, animations and more).

This Repo will only contain the battle UI and little battle logic. Think of it more as a proof of concept/

## How it works

The core of this UI is another project of mine [saltk (SolArus ToolKit)](https://gitlab.com/zitrussaft1/Saltk),
to install it I made use of [sopa](https://gitlab.com/zitrussaft1/sopa).

### Main Circuit

The code runs essentially in two parts:
 - [a map](maps/first_map.lua)
 - and, [a UI file](scripts/test_menu/main.lua)

 First and foremost, let's add a NPC named `fakemon` to the map and set it to call a map script. This will be the Pokemon we will engage in battle.

 ![Solarus Quest Editor UI](pictures/quest-editor.png)

 Then let's create a minimal saltk UI inside the `scripts/test_menu/main.lua` directory. It looks like this:

 ```lua
-- Import saltk
local saltk = require("scripts/saltk/saltk/main")

-- Then we create a function that takes
-- the game as argument. This will be needed
-- as saltk requires `game` as context for drawing
-- and to manage the ui
local function setup_menu(game)
    -- We create a transparent root Widget for our menu
    local root = saltk.Widget.new({
        id="root", -- The id argument should be unique, it's an identifier
        size={320,240}, -- We want the root widget to fill the entire window
        position={0,0}, -- This will position the Widget on the top-left
        scale=true -- ???
    })

    saltk.init(
        game, -- Solarus Game variable
        "test", -- A menu identifier
        root, -- Our newly created widget
        true, -- ???
    )
end

return setup_menu
 ```

The `scale=true` and `true`, the (`saltk.init` parameter), are
two halves of the same whole. These variable are used to tell
saltk to scale the Widgets proportionally to windows size instead
of quest size. Here are the possible scenarios:

- A widget of size 16x16 with `scale=false` and `saltk.init` as `true` will
display a 16x16 image in a 640x480 surface. 
- A widget of size 16x16 with `scale=true` and `saltk.init` as `true` will
scale proportionally to the quest size and display 32x32 image in a 640x480 surface.
This one is specially good for displaying higher resolution images in a quest.
- A widget of size 16x16 with `scale=false` and `saltk.init` as `false` will
display a 16x16 image in a 640x480 surface too
- A widget of size 16x16 with `scale=true` and `saltk.init` as `false` will
display a 16x16 image upscaled to 32x32 in a 640x480 surface. This is Solarus default behavior.

As a rule of thumb, you will like your "root" widget to have `scale=true` and
depending if you are using higher resolution images or not `saltk.init` as `true` or `false`.

Let's now modify our map script so our menu is called:

```lua

-- Get our map and game variables
local map = ...
local game = map:get_game()

-- On map initialization create the menu
function map:on_started()
    -- Import our script, pass game as context
    require("scripts/test_menu/main")(game)
end

-- Now let's add entity behavior 
function fakemon:on_interaction()
    game:set_saltk_enabled( -- The function that starts the menu
        "test", -- Our menu Id
        true -- Enable it, as opposed to disabled
    )
end

```

Awesome! Now if you interact with the "fakemon" your game
should freeze! And you won't be able to unfreeze...

Well, the reason nothing is showing is that we haven't added
anything to the screen! Only a transparent widget!

PS: The initialization of the menu only needs to be called once,
I'm writting it inside `map:on_started` because we won't have 
other maps. Keep this in mind when using `saltk` in other projects.


### Creating the UI

Our UI will have the following components:
- Two pokemons in opposite directions
- An explosion for when damage is dealt
- A Grid with different options
- A textbox narrating what is happening

Let's modify our ui file to do that! 

```lua
local root = saltk.Widget.new({id="root", size={320, 240},
    position={0, 0}, scale=true, can_be_selected=false
})
```

We'll be using the `Image` Widget to display any image inside our `sprites` folder.

```lua
local fakemon1 = saltk.Image.new({
    id="1", image = "fakemon/saltk/ground.png",
    size={128,128}, position={5,60}
})

local fakemon2 = saltk.Image.new({
    id="2", image = "fakemon/saltk/steel.png",
    size={128,128}, position={120,5},
})

local explosion_mon1 = saltk.Image.new({
    id="explosion_mon1", image = "fakemon/saltk/explosion.png",
    size={128,128}, position={5,60},
    visible = false -- This option will hide the image by default
})

local explosion_mon2 = saltk.Image.new({
    id="explosion_mon2", image = "fakemon/saltk/explosion.png",
    size={128,128}, position={120,5},
    visible = false
})
```

Now, let's create the our list of actions

```lua
local actions_grid = saltk.Grid.new({
    id="options",
    grid={1,4}, -- Create a 1 row and 4 columns grid
    padding={4,4}, -- Add padding between rows
    size={80, 160}, 
    position={320-80, 240-160} -- Position it on the bottom
})
```
Let's give it a blue background by adding a blue widget of same size

```lua
local grid_bg = saltk.Widget.new({
    id="bg",
    size={80, 160}, position={320-80, 240-160},
    background_color={0,0,255}
})
```

And finally let's create the text box using a Text Widget

```lua
local battle_log = saltk.Text.new({
    id="battle_log", halign="left",
    valign="top", text_overflow="...",
    overflow=true, font_size=24,
    text="Choose a skill", -- This is the text to be displayed
    padding={0,0},
    size={320-80, 240-160}, position={0,160}
})

local battle_log_bg = saltk.Widget.new({
    id="battle_log_bg",
    size={320-80, 240-160}, position={0,160},
    background_color={125,125,0}
})
```

Add all Widgets to the `root` widget by calling the
`add_child` funtion.

```lua
saltk.Widget.add_child(root, fakemon1)
saltk.Widget.add_child(root, fakemon2)
saltk.Widget.add_child(root, explosion_mon1)
saltk.Widget.add_child(root, explosion_mon2)
saltk.Widget.add_child(root, grid_bg)
saltk.Widget.add_child(root, actions_grid)
saltk.Widget.add_child(root, battle_log_bg)
saltk.Widget.add_child(root, battle_log)
```

That was a lot, but it is not over. We need to create our
buttons. To do so I'll create a list of them:

```lua
local actions = {
        saltk.Text.new({id="fight", halign="center",
            valign="middle", text_overflow="...",
            overflow=false, font_size=8,
            text="Fight",
            padding={0,0},
            -- We will add the `can_be_selected` property
            -- so we can register key presses and mouse
            -- clicks later on.
            can_be_selected=true,
        }),
        ... Other actions
    }
```

Awesome! If you run our program you should now see a complete UI
and if you press `Enter` in some Widget it will print a message
on the console! Nice!

![Resulting UI](pictures/result.png)

You can also use your mouse to select, although the `Enter`
press will still be needed to run the action.

### Building interactions

To do so, we will add two parameters to our `setup_menu` function: The hp of the first pokemon and the hp of the second
pokemon.

```lua
local function setup_menu(game, hp1, hp2)
    local root = ...

    -- Since root is just a table, we will link the health
    -- to the root widget
    root.hp1 = hp1
    root.hp2 = hp2
```

We will now modify these values in our buttons by adding
a `on_key_press` parameter to the widget:

```lua
        saltk.Text.new({id="fight", halign="center",
            ...
            -- A on_key_press function receives two parameters
            -- The widget itself and the key being pressed
            on_key_press = function (widget, key) 
                -- Check if the key is x
                if key == "x" then
                    if root.hp1 > 0 then
                        -- Reduce the HP
                        root.hp1 = root.hp1 - 5
                        -- Update the battle log
                        battle_log.text = "Caused 5 damage to fakemon"
                        -- Display the explosion
                        explosion_mon1.visible = true
                        -- Hide the explosion effect after 1 second
                        sol.timer.start(game, 1000, function ()
                            explosion_mon1.visible = false
                        end)
                    else
                        -- Once done, update the battle log
                        battle_log.text = "Fakemon fainted!"
                        -- Call the close function
                        close()
                    end
                end
            end
        }),
```

You can check the other actions in [this file](data/scripts/test_menu/main.lua).

We also added a `close()` function is used to close the dialog with a nice
fadeout.

```lua
local function close()
    local time = 50
    sol.timer.start(game, 1000, function ()
        -- You can access a Widget's surface directly by
        -- it's .surface property. 
        root.surface:fade_out(time, function () end) 
        fakemon1.surface:fade_out(time, function () end)
        fakemon2.surface:fade_out(time, function () end)
        grid_bg.surface:fade_out(time, function () end)
        actions_grid.surface:fade_out(time, function () end)
        battle_log_bg.surface:fade_out(time, function () end)
        -- When everything fades out, close the menu
        battle_log.surface:fade_out(time, function () 
            -- This function closes the menu
            game:set_saltk_enabled("test", false)
        end)
    end)
end
```

### Accessing and modifying widgets in other portions of the code

This is all good, but because of the fade_out animations whenever you
reopen the menu everything will be transparent! Even worse is that
the pokemons are carrying the health from the previous battle
(although that was expected from the way we coded).

Let's change that!

By modifying our `fakemon:on_interaction` function to reset the state
of our widgets we can fix our transparent images and reset the
pokemon's HP!

```lua
function fakemon:on_interaction()
  -- Get the root widget by calling `get_saltk_widget`
  local widgets = game:get_saltk_widget("test")

  -- Modify the health
  widgets.hp1 = 10
  widgets.hp2 = 5
  if widgets then
    -- Use `saltk.Widget.get_child_by_id` to get all of our widgets
    -- And finally bring them to light using Solarus' builtin `fade_in`
    saltk.Widget.get_child_by_id(widgets, "battle_log").text = "Choose a skill"
    saltk.Widget.get_child_by_id(widgets, "battle_log").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "battle_log_bg").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "options").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "bg").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "1").surface:fade_in(50, function () end)
    saltk.Widget.get_child_by_id(widgets, "2").surface:fade_in(50, function () end)
  end

  -- Display the menu again
  game:set_saltk_enabled("test", true)
end
```

## Final result

![Final Result](pictures/result-fight.png)
(It looks better IRL, but it was difficult to capture the explosion without cutting some parts)
## Conclusion

Saltk and Sopa are very new projects, there are many things that are particularly
important that are still missing but should be coming sooner than the RPG project.
Some of the things that are very cumbersome still are, particularly in Saltk, are:

- [ ] No standard animations (will probably be a separate sub-project), must all be done inside a `render` function for now.
- [ ] No mouse click in Widgets
- [ ] No widget reset functionality, so they return to normal when the menu is called again (Only in cases that this is desired)
- [ ] No documentation

Regardless, it's very cool how it's now possible to use logic similar to Qt and Gtk in Solarus to quickly build user interfaces. I hope they find their way in the Solarus community once more matured :)